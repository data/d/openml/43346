# OpenML dataset: User-OTT-Consumption-Profile---2019

https://www.openml.org/d/43346

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
With the growing dependency that our society has on the Internet, the amount of data that goes through networks keeps increasing. Network monitoring and analysis of consumption behavior represents an important aspect for network operators allowing to obtain vital information about consumption trends in order to offer new data plans aimed at specific users and obtain an adequate perspective of the network. Over-the-top (OTT) media and communications services and applications are shifting the Internet consumption by increasing the traffic generation over the different available networks. OTT refers to applications that deliver audio, video, and other media over the Internet by leveraging the infrastructure deployed by network operators but without their involvement in the control or distribution of the content and are known by their large consumption of network resources.
This dataset presents the summarization of the consumption behavior of users inside Universidad del Cauca Network between different days of April, May and June 2019. The users are classified between Low, Medium and High Consumption users. 
Content
This dataset contains 1249 instances and 114 attributes on a single CSV file. Each instance represents a users consumption profile which holds summarized information about the  consumption behavior of the user related to 56 OTT applications identified in the different IP flows captured in order to create the dataset. The user behavior is summarized with two types of attributes: time occupation (seconds) and data occupation (Bytes) per application.
The application labels include: Amazon, AmazonVideo, Apple, AppleIcloud, AppleItunes, AppleStore, Datasaver, Deezer, Dropbox, eBay, Facebook, GMail, Google, GoogleDocs, GoogleDrive, GoogleHangoutDuo, GoogleMaps, GooglePlus, GoogleServices, HTTP and HTTPProxy (Browsing), IMO, Instagram, Linkedin, LotusNotes, Messenger, MSOneDrive, MSN, NetFlix, Playstation, PlayStore, PlayStation_VUE,  Sina (weibo), Skype, SkypeCall, Snapchat, Soundcloud, Spotify, Steam, TeamViewer, Telegram, TikTok, Tuenti, Twitch, Twitter, Viber, Waze, WeChat, WhatsApp, WhatsAppCall, WhatsAppFiles, Wikipedia, Xbox, Yahoo, YouTube, Zoom.
For further information you can read and please cite the following papers:
IEEE Xplore: https://ieeexplore.ieee.org/document/9258898
Research Gate: https://www.researchgate.net/publication/345990587_Smart_User_Consumption_Profiling_Incremental_Learning-based_OTT_Service_Degradation
Acknowledgements
I would like to thank Universidad Del Cauca and MEDIANETS Group from Budapest University of Technology and Economics for supporting this research and MinCiencias for my PhD scholarship.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43346) of an [OpenML dataset](https://www.openml.org/d/43346). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43346/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43346/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43346/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

